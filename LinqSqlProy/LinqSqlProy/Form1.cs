﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqSqlProy
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CompanyDBDataContext dc = new CompanyDBDataContext();
            //Si la tabla es Employee, el método que devuelve la tabla sera en plural Employees
            /*MÉTODO UNO*/
            //GridEmployee.DataSource = dc.Employees;
            /*MÉTODO DOS*/
            System.Data.Linq.Table < Employee > tablaEmployee = dc.Employees;
            GridEmployee.DataSource = tablaEmployee;
        }
    }
}
