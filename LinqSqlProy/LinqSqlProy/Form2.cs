﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqSqlProy
{
    public partial class Form2 : Form
    {
        CompanyDBDataContext dc;
        List<Employee> emp;
        int rno = 2;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            dc = new CompanyDBDataContext();
            Table<Employee> tab = dc.Employees;
            emp = new List<Employee>();
            emp = tab.ToList();
            ShowData();
        }
        private void ShowData()
        {
            enoBox.Text = emp[rno].Eno.ToString();
            enameBox.Text = emp[rno].Ename;
            jobBox.Text = emp[rno].Job;
            salaryBox.Text = emp[rno].Salary.ToString();
            dnameBox.Text = emp[rno].Dname.ToString();
        }

        private void prevBtn_Click(object sender, EventArgs e)
        {
            if (rno > 0)
            {
                rno--;
                ShowData();
            }
            else
            {
                MessageBox.Show("Primera fila de tabla");
            }
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            if(rno < emp.Count-1)
            {
                rno++;
                ShowData();
            }
            else
            {
                MessageBox.Show("ültima fila de la tabla");
            }
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
