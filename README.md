# VISUAL STUDIO 2017

## C SHARP

### Estructura de un proyecto

Para ejecutar directamente el proyecto se ejecuta el archivo con extensión ***.sln***

### Mostrar datos por consola

Para mostrar datos por consola se escribe la siguiente sintaxis:

~~~csharp
Console.WriteLine("Hola mundo");
~~~

La siguiente sentencia, lee una tecla. Esto para cuando se muestre la consola de comandos, esta se detenga hasta que se escriba un letra.

~~~csharp
Console.ReadKey();
~~~

La sintaxis completa de nuestro primer programa sería:

~~~csharp
namespace Presentacion1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola mundo");
            Console.ReadKey();
        }
    }
}
~~~


### Tipos de datos

~~~csharp
int x1 = 5;
decimal x2 = 0.02m;
float f = 0.0f;
double d = 0.003d;
string cadena = "Hola mundo";
bool bandera = true;//or false
DateTime fecha = DateTime.Now;//Nos devuelve la fecha actual

//Mostrando los datos por consola
Console.WriteLine("Numero entero {0} - {1} - {2:f} - {3}", x1, x2, f, d);
Console.WriteLine(cadena);
Console.WriteLine(bandera);
Console.WriteLine(fecha);
~~~

### Conversiones de tipo

No se pueden convertir los valores grandes a valores pequeños.

~~~csharp
			int i = 10;
            decimal x = 4.5m;
            bool bandera = false;
            string cadena = string.Empty;//inicializa una cadena vacia
            DateTime fecha = DateTime.Now;
            
			//Conversion mediante CAST de decimal a entero
            i = (int)x;

            //Conversion mediante la clase Convert de entero a decimal
            i = Convert.ToInt16(x);
			// i = int.Parse(x);
            Console.WriteLine(i);

            //Conversión de string a entero
            string numeroS = "123";
            int numeroI = Convert.ToInt32(numeroS);
            Console.WriteLine(numeroI);

            //Conversion de bool a string
            Console.WriteLine(bandera.ToString());

            //Conversión de la fecha
            Console.WriteLine(fecha.ToShortTimeString());
~~~

### Introducir datos por consola

Para meter datos por teclado mediante la consola, se escribe la siguiente sentencia:

~~~csharp
string var = Console.ReadLine();
~~~

El valor devuelto por la sentencia ***Console.ReadLine()*** es de tipo ***string***.

### Condicional IF

~~~csharp
			if (a>b)
            {
                Console.WriteLine("a = {0} es mayor que b = {1}", a, b);
            }
            else if(b>a)
            {
                Console.WriteLine("b = {1} es mayor que a = {0}", a, b);
            }
            else if(a==b)
            {
                Console.WriteLine("b = {1} es igual que a = {0}", a, b);
            }
~~~

## Programación Orientada a objetos

### Estructura básica de una Clase

La estructura básica de una clase es la siguiente:

~~~csharp
namespace Clases
{
    public class Empleados
    {
        //Atributos
		//Métodos
    }
}
~~~

### Declaración de un constructor

~~~csharp
		public Empleados()
        {
            string nombre = "vacio";
            decimal sueldoDiario = 0.0m;
            int edad = 0;
        }
~~~

### Método GET y SET para obtener y asignar un atributo

~~~csharp
		public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
~~~

Una forma abreviada de hacer esto es escribir ***prop*** y presionar la tecla ***TAB*** dos veces ó presionando la combinación de teclas ***CTRL*** + ***R*** + ***E***.

Dentro la clase Main para llamar a este método se sigue la siguiente sentencia:

~~~csharp
//Para asignar una valor a la variable
e.Nombre = "Rafael Ortiz";

//Para mostrar el valor de la variable
Console.WriteLine("Nombre = "+e.Nombre);
~~~

### HERENCIA

Para el siguiente ejemplo creamos la clase padre ***Cliente***:

~~~csharp
namespace Herencia
{
    public class Cliente
    {
        private int id;
        private string nombre;
        private string apellidos;

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
    }
}
~~~

Ahora creamos la clase hija ***ClientesVentas***

~~~csharp
namespace Herencia
{
    public class ClientesVentas: Cliente //La clase ClientesVentas hereda de Cliente
    {
        private string rfc;
        private string direccion;
        private string colonia;
        private string municipio;
        private bool esCredito;

        public string Rfc { get => rfc; set => rfc = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Colonia { get => colonia; set => colonia = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public bool EsCredito { get => esCredito; set => esCredito = value; }
    }
}
~~~

### CLASES ABSTRACTAS

Una clase abstracta es un clase que no se puede instanciar.

Un método sin cuerpo es conocido como un método abstracto, que contiene solo la declaración del método, como por ejemplo:

~~~csharp
abstract class ClientesAbstract //Declaración de una clase abstracta
{
	//Funcion 1
	public abstract void Imprime(); //Un método abstracto sin parámetros, ni valores de retorno.
	//Funcion 2
	public abstract int numero(int x);//Función con valores con parámetros y valores de retorno
}
~~~

Si el método es declarado como abstracto bajo alguna clase, entonces las clases hijas son responsables por implementar esté método, como se ve en el siguiente ejemplo:

~~~csharp
	public class ClienteBase : ClientesAbstract
    {
		//Funcion 1
        public override void Imprime()
        {
            Console.WriteLine("Sobreescribiendo el método de la clase abstracta");
        }
		//Funcion 2
		public override int numero(int k)
        {
            return k * 2;
        }
    }
~~~

### POLIMORFISMO

#### OVERRIDE(Sobre escritura)

Es un enfoque para reimplementar un método de la clase padre dentro la clase hija con el mismo nombre.

En este caso se definirá múltiples métodos con el mismo nombre, pero con los mismos parámetros.

Estas se pueden ejecutar dentro las clases padres e hijas, pero nunca se puede ejecutar dentro la misma clase.

Para la sobrescritura de métodos la clase hija necesita permisos de la clase padre, para lo que se utilizá la palabra clave ***virtual*** de la siguiente forma:

~~~csharp
class LoadParent
    {
        public virtual void show() //Vurtual hace que este metodo sea overridible
        {
            Console.WriteLine("Parent´s show method is called");
        }
        public void Test()
        {
            Console.WriteLine("Parent´s Test method is called");
        }
    }
~~~

Y para sobreescribirla en la clase hija el utiliza la palabra clave ***override***.

~~~csharp
class LoadChild: LoadParent
    {
        public override void show() //Override dice que este método se puede sobreescribir.
        {
            Console.WriteLine("Este método es llamado desde la clase hij@");
        }
    }
~~~

#### SOBRECARGA

En este caso se definirá múltiples métodos con el mismo nombre, pero con diferentes parámetros.

Esta puede ser ejecutada ya sea dentro como también entre sus clases padres e hijas.

Para la sobrecarga de métodos la clase hija no necesita permisos de la clase padre.

##### Sobrecarga de Constructores

~~~csharp
			Parentload pl1 = new Parentload();
            Parentload pl2 = new Parentload(5);
            Console.WriteLine(pl1.Id);
            Console.WriteLine(pl2.Id);
~~~

##### Sobrecarga de Métodos

~~~csharp
			pl1.cambiaId(pl1.Id, 3);
            pl2.cambiaId(pl2.Id, 4);
            Console.WriteLine(pl1.Id);
            Console.WriteLine(pl2.Id);
~~~

### INTERFACES

#### Similitudes entre Clases e Interfaces

Una ***Clase*** es un tipo de dato definido por el usuario.

Una ***Interface*** es también un tipo de dato definido por usuario.

#### Diferencias entre Clases e Interfaces

Las ***Clase*** no pueden contener métodos abstractos (métodos con sin cuerpo).

Las ***Clases Abstractas*** pueden contener métodos abstractos (métodos sin cuerpo) y además pueden contener métodos con cuerpo.

Las ***Interfaces*** solo puede contener métodos abstractos (sin cuerpo). Cada método abstracto de la interface debe ser obligatoriamente implementado por la clase hija de dicha Interface.

Generalmente cuando una clase hereda de otra clase, es para consumir los miembros de la clase padre. Pero cuando una clase hereda de una Interface para implementar los miembros de dicha Interface.

- Una clase puede heredar de una clase e interface a la vez.
- No se puede declara ningúna campo/variable dentro la interface.
- Una interface puede heredar de otra interface.

#### Estructura de una Clase y una Interface

~~~
//CLASE
[<modifiers>] class <Name>
{
	/*Declaración de miembros*/
}

//INTERFACE
[<modifiers>] interface <Name>
{
	/*Declaración de miembros abstractos*/
}
~~~

El alcance de los miembros de una interface es de tipo publico, mientras que es privado en el caso de una clase, como se ve el siguiente ejemplo:

~~~csharp
	interface ITestInterface
    {
        int suma(int a, int b); //Por defecto es public  
    }
~~~

Por lo tanto, no se declara:

~~~csharp
	public abstract int suma(int a, int b);
~~~	

#### EJEMPLO

En el siguiente ejemplo declaramos la interface ***ITestInterface1***, con el método ***sub***.

~~~csharp
	interface ITestInterface1
    {
        int sub(int a, int b); 
    }
~~~

Se crea la interface ***ITestInterface2***, la cual hereda de ***ITestInterface1*** con método ***add***, como se muestra a continuación:

~~~csharp
	interface ITestInterface2: ITestInterface1
    {
        int add(int a, int b); 
    }
~~~

Finalmente se crea la clase ***ImplementationClass*** la cual heredara de la interface ***ITestInterface2***, la que a su vez heredara de la interface ***ITestInterface1***, por lo que se deberá implementar los métodos de ambas Interfaces, se puede implementar los métodos de las dos siguientes formas:

- La Forma 1 hace referencia a la interface, por lo que no es necesario colocar la palabra ***public***.

- En la Forma 2 si es necesario colocar la modificador public, ya que la clase por defecto asigna el modificador private.

~~~csharp
	class ImplementationClass : ITestInterface2
    {
		//Forma 1
        int ITestInterface1.sub(int a, int b)
        {
            return a - b;
        }
		//Forma 2
        public int add(int a, int b)
        {
            return a + b;
        }
    }
~~~

Para instanciar la clase en el programa principal, se puede hacer de dos formas:

~~~csharp
		ImplementationClass ia = new ImplementationClass();
        Console.WriteLine(ia.sub(6, 4));
        Console.WriteLine(ia.add(3, 4));
~~~

La segunda forma sería:

~~~csharp
			ImplementationClass ia = new ImplementationClass();
            ITestInterface1 obj1 = ia;
            Console.WriteLine(obj1.sub(6, 2));
            ITestInterface2 obj2 = ia;
            Console.WriteLine(obj2.add(6, 2));
~~~

#### Herencia múltiples con interfaces

La herencia múltiple a través de las clases, no está soportada en CSharp (Una clase solo puede tener una y solo una clase padre), mientras que la misma clase puede tener cualquier número de interfaces como padre(s).

En el siguiente ejemplo se tiene las siguiente interfaces:

~~~csharp
	interface Interface1
    {
        void Test();
    }
    interface Interface2
    {
        void Test();
    }
~~~

Donde la clase MultipleInheritanceTest hereda de ambas interfaces (herencia múltiple):

~~~csharp
	class MultipleInheritanceTest : Interface1, Interface2
    {
        void Interface1.Test()
        {
            Console.WriteLine("Método Interface 1");
        }
        void Interface2.Test()
        {
            Console.WriteLine("Método Interface 2");
        }
    }
~~~

A continuación se llama dichos métodos dentro de la clase principal:

~~~csharp
 			MultipleInheritanceTest obj = new MultipleInheritanceTest();
            Interface1 i1 = obj;
            Interface2 i2 = obj;
            i1.Test();
            i2.Test();
            Console.ReadKey();
~~~

### EXCEPCIONES

Cuando se desarrolla una aplicación puede haber dos tipos de errores:

1. **Errores de compilación.-** Los errores de compilación puede ocurrir por la falta de un punto y como, o por no haber cerrado  paréntesis, comillas o corchetes.
2. **Errores en tiempo de ejecución (Runtime errors).-** Los errores en tiempo de ejecución puede ocurrir por:
- Una mala implementación de la lógica (Por ejemplo: división entre cero).
- Por una entrada de datos incorrecta.
- Recursos requeridos incorrectos.

Una excepción es una clase, la cual es la responsable de la terminación normal de un programa.

La clase ***Exception*** tiene una propiedad de solo lectura para mostrar un mensaje de error, la cual es declarada como virtual, lo que permite a la clases hijas sobreescribir dicho método, el nombre de esta propiedad es **message**.

Bajo esta clase ***Exception*** se definen dos clases:

- **ApplicationException** son excepciones definidas por el programador.
- **SystemException** son excepciones que vienen ya definidas dentro el lenguaje y son ya conocidas como la división entre cero.

#### Tipos de excepción

- IndexOutOfBoundsException (Cuando se excede los limites del array)
- DivideByZeroException (División entre cero)
- OverFlowException
- FormatExceptioon

#### Estructura de una Excepción

El bloque de código está estructurado de la siguiente forma:

~~~csharp
			try
            {
                Console.WriteLine("Número 1");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número 2");
                int y = Convert.ToInt32(Console.ReadLine());
                int z = x / y;
                Console.WriteLine("El resultado es {0}", z);
            }
            catch(DivideByZeroException ex1)
            {
                Console.WriteLine(ex1.Message);
            }
            catch(FormatException ex2)
            {
                Console.WriteLine(ex2.Message);
            }
            catch(Exception e)//Atrapá todo tipo de excepciones
            {
                Console.WriteLine(e.Message);
            }
			finally //Este bloque de código siempre se ejecutá
            {
                Console.WriteLine("Este mensaje siempre se ejecutará");
            }
~~~


#### Lanzando excepciones con THROW

Como se vio, existen dos clases que heredan de la clase **Excepcion**:

- ApplicationException
- SystemException

Como se vio anteriormente las excepciones ApplicationException, son las excepciones que define el desarrollador.

A continuación se verá un caso en el que el desarrollador deberá definir un excepción exclusiva en su aplicación.

En el anterior ejemplo se tenia las siguientes excepciones:

- FormatException para verificar que la entrada de datos sea numérica.
- DivideByZeroException para que no exista división entre cero.
- Exception Para atrapara cualquier otro tipo de excepción no esperado. 

Lo que haremos ahora, será definir nuestra propia excepción para que la división no se la puede hacer entre números impares, como se ve en el ejemplo:

~~~csharp
				Console.WriteLine("Número 1");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número 2");
                int y = Convert.ToInt32(Console.ReadLine());
                if (y % 2 > 0)
                {
                    throw new ApplicationException("El divisor no puede ser un número impar");
                }
                int z = x / y;
                Console.WriteLine("El resultado es {0}", z);
~~~

En la linea ***throw new ApplicationException("El divisor no puede ser un número impar");*** se lanza la excepción cuando el divisor sea un número impar, y nos devuelve como resultado:

~~~
System.ApplicationException: 'El divisor no puede ser un número impar'
~~~

#### Creando nuestras propias excepciones

Para crear nuestra propia excepcion, tendremos que crear una clase (DivideByOddNoException) que herede de la clase ***ApplicationException*** y sobrescribir el método de solo lectura ***Message***:

~~~csharp
	class DivideByOddNoException: ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Attempted to divide by odd number";
            }
        }
    }
~~~

Dentro el programa principal, para lanzar la excepción:

~~~csharp
				Console.WriteLine("Número 1");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número 2");
                int y = Convert.ToInt32(Console.ReadLine());
                if (y % 2 > 0)
                {
                    throw new DivideByOddNoException();
                }
                int z = x / y;
                Console.WriteLine("El resultado es {0}", z);
~~~

Finalmente para atrapar nuestra excepción usamos ***catch***:

~~~csharp
			try
            {
                Console.WriteLine("Número 1");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número 2");
                int y = Convert.ToInt32(Console.ReadLine());
                if (y % 2 > 0)
                {
                    //throw new ApplicationException("El divisor no puede ser un número impar");
                    throw new DivideByOddNoException();
                }
                int z = x / y;
                Console.WriteLine("El resultado es {0}", z);
            }
            catch(DivideByZeroException ex1)
            {
                Console.WriteLine(ex1.Message);
            }
            catch(DivideByOddNoException ex2) //Atrapando nuestra excepción
            {
                Console.WriteLine(ex2.Message);
            }
            catch(FormatException ex3)
            {
                Console.WriteLine(ex3.Message);
            }
            catch(Exception ex4)
            {
                Console.WriteLine(ex4.Message);
            }
            Console.WriteLine("Fin del programa");
            Console.ReadKey();
~~~

### ARREGLOS

Para definir un array de valores si sigue la siguiente sintaxis:

~~~csharp
int[] Arreglo = new int[4];
~~~

El array de arriba de tipo int y tiene una dimensión de 4 elementos.

Para definir un array de objetos:

~~~csharp
Numero[] numV = new Numero[5];
~~~

### ARREGLOS MULTIDIMENSIONALES

Ahora declaramos una array multidimensional de tipo ***int***.

~~~csharp
int[,] matriz = new int[4, 3];
~~~

### ARRAYLIST

Anteriormente se vio que los arrays, son estructuras de datos del mismo tipo, ahora se extenderá esa información a lo que son colecciones:

Las colecciones al igual que en los arrays, podremos acceder a sus elementos a través de sus indices. 

Las ventajas que tienen estas colecciones frente a los arrays, es que son estructuras que tienen tamaño variable y además tienen métodos que nos van a permitir manejar dichas estructuras.

Declara un ArrayList:

~~~csharp
ArrayList lista = new ArrayList();
~~~

Acionamos elementos dentro este ArrayList:

~~~csharp
lista.Add(elemento);
~~~

Para saber el número de elementos que tiene:

~~~csharp
lista.Count
~~~

Para hacer el recorrido del ArrayList:

~~~csharp
foreach (Object item in lista)
{
	Console.WriteLine(item);
}
~~~

### LINQ TO SQL 

Microsoft en Dodnet 3.5 añadió el concepto de LINQ (Lenguage Integrated Query). Existen dos tipos de LINQ:

- Linq to Objects: Arrays, Collections, etc.
- Linq to Database: DataTables, Relational Database tables.
	- Linq to ADO.net.- Podemos escribir querys en las tablas.
	- Linq to SQL.- Podemos escribir querys en base de datos relacionales (solo en sql server).
	- Linq to Entities.- Podemos escribir querys en sql server y otros bases de datos, como Oracle.
- Linq to XML: XML Files.

### LIST

Una lista es una colección generica de clases.

Instanciamos una lista que solo contendrá valores enteros:

~~~csharp
//Lista de valores enteros
List<int> li = new List<int>();

//Lista de cadenas
List<string> li = new List<string>();
~~~

#### Linq to SQL

Es un lenguaje query que fue introducido a partir del framework .net 3.5 para trabajar con base de datos relacional solo en SQl Server, no en otras bases de datos.

Nota: Ademas podemos llamar procedimientos almacenados usando Linq to SQL.

**SQL => SQL Server**

1. Verificación de la sintaxis en tiempo de ejecución de las declaraciones SQL.
2. Not type safe
3. No intellisense Support
4. Debuguear las declaraciones sql no es posible.
5. El código es una combinacion de objetos orientados y relacionados.

**LINQ => SQL Server**

1. Verifica sintaxis en tiempo de compilación.
2. Type safe.
3. Intellisense Support is Available.
4. Debug de Linq sql es posible.
5. Puro código orientado a objetos (
	- Cada tabla es una clase.
	- Cada columna es una propiedad de la clase.
	- Filas o registros es una instancia de la clase.
	- Procedimientos almacenados serán métodos de la clase.

Para trabajar con Linq Sql, primero necesitamos convertir todos los objetos relacionados de la base de datos en tipos orientados a objetos y este proceso es conocido como ORM (Object Relational Mapping).

Para ejecutar ORM se nos proporciona una herramienta conocida como OR Designer. 

















